#!/bin/bash

kubectl delete -f ./leohotest/1226-statefulset/kafka/   # Kafka
kubectl delete -f ./leohotest/1226-statefulset/mqtt/    # Mosquitto
kubectl delete -f ./leohotest/1226-statefulset/mariadb/ # MariaDB
kubectl delete -f ./leohotest/1226-statefulset/redis/   # Redis
kubectl delete -f ./leohotest/1226-statefulset/web/     # Web