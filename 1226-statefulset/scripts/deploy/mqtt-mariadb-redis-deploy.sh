#!/bin/bash

kubectl apply -f ./leohotest/1226-statefulset/mqtt/    # Mosquitto
kubectl apply -f ./leohotest/1226-statefulset/mariadb/ # MariaDB
kubectl apply -f ./leohotest/1226-statefulset/redis/   # Redis