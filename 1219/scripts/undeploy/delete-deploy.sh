#!/bin/bash

kubectl delete -f ./leohotest/1219/kafka/   # Kafka
kubectl delete -f ./leohotest/1219/mqtt/    # Mosquitto
kubectl delete -f ./leohotest/1219/mariadb/ # MariaDB
kubectl delete -f ./leohotest/1219/redis/   # Redis
kubectl delete -f ./leohotest/1219/web/     # Web