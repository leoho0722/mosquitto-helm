#!/bin/bash

kubectl delete -f ./leohotest/mqtt/    # Mosquitto
kubectl delete -f ./leohotest/mariadb/ # MariaDB
kubectl delete -f ./leohotest/redis/   # Redis