#!/bin/bash

kubectl apply -f ./leohotest/1219/kafka/01-zookeeper.yaml       # ZooKeeper
kubectl apply -f ./leohotest/1219/kafka/02-kafka-broker-1.yaml  # Kafka Broker 1
kubectl apply -f ./leohotest/1219/kafka/02-kafka-broker-2.yaml  # Kafka Broker 2
kubectl apply -f ./leohotest/1219/kafka/03-schema-registry.yaml # Schema Registry
kubectl apply -f ./leohotest/1219/kafka/04-connect.yaml         # Kafka Connect
kubectl apply -f ./leohotest/1219/kafka/05-ksqldb-server.yaml   # KSQLDB Server
kubectl apply -f ./leohotest/1219/kafka/06-control-center.yaml  # Control Center
kubectl apply -f ./leohotest/1219/kafka/07-ksqldb-cli.yaml      # KSQLDB CLI
kubectl apply -f ./leohotest/1219/kafka/08-ksql-datagen.yaml    # KSQLDB DateGen
kubectl apply -f ./leohotest/1219/kafka/09-rest-proxy.yaml      # Confluent REST Proxy