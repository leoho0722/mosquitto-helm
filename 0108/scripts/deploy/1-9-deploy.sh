#!/bin/bash

kubectl apply -f ./leohotest/0108/kafka/01-zookeeper.yaml       # ZooKeeper
kubectl apply -f ./leohotest/0108/kafka/02-broker.yaml          # Kafka Broker
kubectl apply -f ./leohotest/0108/kafka/03-schema-registry.yaml # Schema Registry
kubectl apply -f ./leohotest/0108/kafka/04-connect.yaml         # Kafka Connect
kubectl apply -f ./leohotest/0108/kafka/05-ksqldb-server.yaml   # KSQLDB Server
kubectl apply -f ./leohotest/0108/kafka/06-control-center.yaml  # Control Center
kubectl apply -f ./leohotest/0108/kafka/07-ksqldb-cli.yaml      # KSQLDB CLI
kubectl apply -f ./leohotest/0108/kafka/08-ksql-datagen.yaml    # KSQLDB Datagen
kubectl apply -f ./leohotest/0108/kafka/09-rest-proxy.yaml      # Confluent REST Proxy