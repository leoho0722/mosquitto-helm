#!/bin/bash

kubectl delete -f ./leohotest/0108/kafka/   # Kafka
kubectl delete -f ./leohotest/0108/mqtt/    # Mosquitto
kubectl delete -f ./leohotest/0108/mariadb/ # MariaDB
kubectl delete -f ./leohotest/0108/redis/   # Redis
kubectl delete -f ./leohotest/0108/web/     # Web