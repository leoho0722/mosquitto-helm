# 要安裝的 Helm chart 名稱
HELM_CHART_NAME ?= mosquitto-test

# 是否為本地測試
LOCAL_TEST ?= false

# 檢查 Helm chart 格式是否有錯誤
.PHONY: helm-lint
helm-lint:
	helm lint ${HELM_CHART_NAME}

# 打包 Helm chart
.PHONY: helm-package
helm-package:
	helm package ${HELM_CHART_NAME}

# 安裝 Helm chart Release 版本
.PHONY: helm-install
helm-install:
ifeq ($(LOCAL_TEST), true)
	helm install ${HELM_CHART_NAME} /Users/leoho/Desktop/mosquitto-helm/${HELM_CHART_NAME}
else
	helm install ${HELM_CHART_NAME} leoho0722/${HELM_CHART_NAME}
endif

# 更新 Helm chart Release 版本
.PHONY: helm-upgrade
helm-upgrade:
ifeq ($(LOCAL_TEST), true)
	helm upgrade ${HELM_CHART_NAME} /Users/leoho/Desktop/mosquitto-helm/${HELM_CHART_NAME}
else
	helm upgrade ${HELM_CHART_NAME} leoho0722/${HELM_CHART_NAME}
endif

# 刪除 Helm chart Release 版本
.PHONY: helm-uninstall
helm-uninstall:
	helm uninstall ${HELM_CHART_NAME}